# unittest

可能限り、unittestを行う
credit.rakutenを参考にするとよい

## 実行コマンド
dockerを立ち上げて実行する

```zsh
pwd
>> /home/agg_scraping/src

python -m unittest -v tests/{任意のパス}
```

## testsディレクトリ
testとテストデータは、`/aggregator/src/tests`に記述する

testsも同様にカテゴリで分類する
```
.agg_scraping/src/tests
├── bank
├── credit
├── debit
├── emoney
└── services
```

## テストデータの用意
webサイトのhtmlファイルを`/html`に保存する
年月日をファイル名に入れる

ex) `/agg_scraping/src/tests/credit/rakuten/html/20210601.test.html`


## テスト用htmlのパス
driverに、保存したhtmlを読み込ませる場合のパスは、以下となる
`file:///home/agg_scraping/src/tests/credit/rakuten/html/20210601.test.html`
